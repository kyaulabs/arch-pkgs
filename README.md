![arch-pkgs](https://www.archlinux.org/static/logos/archlinux-logo-dark-scalable.svg "Arch Linux Packages")  
<a href="https://aur.archlinux.org/">https://aur.archlinux.org</a>

[![](https://img.shields.io/badge/coded_in-vim-green.svg?logo=vim&logoColor=brightgreen&colorB=brightgreen&longCache=true&style=flat)](https://vim.org)  

### About
This is a collection of personally modified or created packages for Arch Linux. If you should choose to use any of these packages, treat them as you would any other package from the AUR.

### Install Package
To install any of the packages, one would issue the following commands:

```shell
# cd <package>
# makepkg -sri
```
